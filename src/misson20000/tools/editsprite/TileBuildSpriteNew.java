package misson20000.tools.editsprite;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

public class TileBuildSpriteNew extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5434639443903127143L;
	private final JPanel contentPanel = new JPanel();
	private JSpinner hspinner;
	private JSpinner wspinner;
	private JTabbedPane tabs;

	/**
	 * Create the dialog.
	 */
	public TileBuildSpriteNew(JTabbedPane tabs) {
		this.tabs = tabs;
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_contentPanel.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_contentPanel.columnWeights = new double[] { 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_contentPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblSpriteWidth = new JLabel("Sprite Width");
			lblSpriteWidth.setMinimumSize(lblSpriteWidth.getPreferredSize());
			GridBagConstraints gbc_lblSpriteWidth = new GridBagConstraints();
			gbc_lblSpriteWidth.anchor = GridBagConstraints.WEST;
			gbc_lblSpriteWidth.insets = new Insets(0, 0, 5, 5);
			gbc_lblSpriteWidth.gridx = 1;
			gbc_lblSpriteWidth.gridy = 0;
			contentPanel.add(lblSpriteWidth, gbc_lblSpriteWidth);
		}
		{
			wspinner = new JSpinner();
			wspinner.setModel(new SpinnerNumberModel(16, 1, 256, 1));
			wspinner.setMinimumSize(wspinner.getPreferredSize());
			GridBagConstraints gbc_wspinner = new GridBagConstraints();
			gbc_wspinner.fill = GridBagConstraints.HORIZONTAL;
			gbc_wspinner.anchor = GridBagConstraints.EAST;
			gbc_wspinner.insets = new Insets(0, 0, 5, 0);
			gbc_wspinner.gridx = 2;
			gbc_wspinner.gridy = 0;
			contentPanel.add(wspinner, gbc_wspinner);
		}
		{
			JLabel lblSpriteHeight = new JLabel("Sprite Height");
			lblSpriteHeight.setMinimumSize(lblSpriteHeight.getPreferredSize());
			GridBagConstraints gbc_lblSpriteHeight = new GridBagConstraints();
			gbc_lblSpriteHeight.insets = new Insets(0, 0, 5, 5);
			gbc_lblSpriteHeight.anchor = GridBagConstraints.WEST;
			gbc_lblSpriteHeight.gridx = 1;
			gbc_lblSpriteHeight.gridy = 1;
			contentPanel.add(lblSpriteHeight, gbc_lblSpriteHeight);
		}
		{
			hspinner = new JSpinner();
			hspinner.setModel(new SpinnerNumberModel(16, 1, 256, 1));
			hspinner.setMinimumSize(hspinner.getPreferredSize());
			GridBagConstraints gbc_hspinner = new GridBagConstraints();
			gbc_hspinner.fill = GridBagConstraints.HORIZONTAL;
			gbc_hspinner.anchor = GridBagConstraints.EAST;
			gbc_hspinner.insets = new Insets(0, 0, 5, 0);
			gbc_hspinner.gridx = 2;
			gbc_hspinner.gridy = 1;
			contentPanel.add(hspinner, gbc_hspinner);
		}
		contentPanel.setMinimumSize(contentPanel.getPreferredSize());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("Ok");
				okButton.addActionListener(this);
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				cancelButton.addActionListener(this);
				buttonPane.add(cancelButton);
			}
		}
		this.pack();
		this.setMinimumSize(this.getSize());
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand() == "Ok") {
			TileBuildSpriteEdit e;
			tabs.add("New Sprite", e = new TileBuildSpriteEdit());
			Sprite s = e.getSprite();
			int w = s.width = (Integer) wspinner.getModel().getValue();
			int h = s.height = (Integer) hspinner.getModel().getValue();
			e.addFrame(new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB));
			e.invalidateViewer();
		} else {
		}
		this.setVisible(false);
	}
}
