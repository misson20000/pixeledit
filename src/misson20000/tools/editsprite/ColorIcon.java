package misson20000.tools.editsprite;

import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;

public class ColorIcon implements Icon {
	public void paintIcon(Component c, Graphics g, int x, int y) {
		g.setColor(c.getForeground());
		g.fillRect(x, y, 30, 15);
	}

	@Override
	public int getIconHeight() {
		return 15;
	}

	@Override
	public int getIconWidth() {
		return 30;
	}
}
