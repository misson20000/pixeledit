package misson20000.tools.editsprite;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JApplet;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;

public class TileBuildApplet extends JApplet implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3899770987978377573L;
	private JFileChooser fc;
	private JTabbedPane tabs;

	/**
	 * Create the applet.
	 */
	public TileBuildApplet() {
		getContentPane().setLayout(new BorderLayout(0, 0));

		JMenuBar menuBar = new JMenuBar();
		getContentPane().add(menuBar, BorderLayout.NORTH);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenu mnNew = new JMenu("New");
		mnFile.add(mnNew);

		JMenuItem mntmSpriteSet = new JMenuItem("Sprite...");
		mntmSpriteSet.setActionCommand("new-sprite");
		mntmSpriteSet.addActionListener(this);
		mnNew.add(mntmSpriteSet);

		JMenuItem mntmTileSet = new JMenuItem("Tile Set...");
		mnNew.add(mntmTileSet);

		JMenuItem mntmOpen = new JMenuItem("Open...");
		mnFile.add(mntmOpen);

		JMenuItem mntmImportAsFrame = new JMenuItem("Import as Frame...");
		mntmImportAsFrame.setActionCommand("import-sprite-frame");
		mntmImportAsFrame.addActionListener(this);
		mnFile.add(mntmImportAsFrame);

		JMenuItem mntmImportSpritesheet = new JMenuItem("Import Spritesheet...");
		mntmImportSpritesheet.setActionCommand("import-sprite-sheet");
		mntmImportSpritesheet.addActionListener(this);
		mnFile.add(mntmImportSpritesheet);

        JMenuItem mntmReferenceImage = new JMenuItem("Load reference image...");
        mntmReferenceImage.setActionCommand("load-reference");
        mntmReferenceImage.addActionListener(this);
        mnFile.add(mntmReferenceImage);

		JMenuItem mntmSave = new JMenuItem("Save...");
		mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				InputEvent.CTRL_MASK));
		mnFile.add(mntmSave);

		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);

		JMenuItem mntmUndo = new JMenuItem("Undo");
		mntmUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,
				InputEvent.CTRL_MASK));
		mnEdit.add(mntmUndo);

		JMenuItem mntmCopy = new JMenuItem("Copy");
		mntmCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,
				InputEvent.CTRL_MASK));
		mnEdit.add(mntmCopy);

		JMenuItem mntmPaste = new JMenuItem("Paste");
		mntmPaste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,
				InputEvent.CTRL_MASK));
		mnEdit.add(mntmPaste);

		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);

		JMenuItem mntmAbout = new JMenuItem("About");
		mnHelp.add(mntmAbout);

		tabs = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(tabs, BorderLayout.CENTER);

		fc = new JFileChooser();
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand().equals("import-sprite-frame")) {
			if (tabs.getSelectedComponent() instanceof TileBuildSpriteEdit) {
				int result = fc.showOpenDialog(this);
				if (result == JFileChooser.APPROVE_OPTION) {
					try {
						BufferedImage i = ImageIO.read(fc.getSelectedFile());
						TileBuildSpriteEdit e = (TileBuildSpriteEdit) tabs
								.getSelectedComponent();
						if (i.getWidth() > e.getSprite().width) {
							e.getSprite().width = i.getWidth();
						}
						if (i.getHeight() > e.getSprite().height) {
							e.getSprite().height = i.getHeight();
						}
						e.addFrame(i);
						e.invalidateViewer();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		if (evt.getActionCommand().equals("import-sprite-sheet")) {
			int result = fc.showOpenDialog(this);
			if (result == JFileChooser.APPROVE_OPTION) {
				TileBuildSpriteSheetImport d = new TileBuildSpriteSheetImport(
						fc, tabs);
				d.setVisible(true);
			}
		}
		if(evt.getActionCommand().equals("new-sprite")) {
			new TileBuildSpriteNew(tabs).setVisible(true);
		}
        if(evt.getActionCommand().equals("load-reference")) {
            int result = fc.showOpenDialog(this);
            if(result == JFileChooser.APPROVE_OPTION) {
                Sprite s = ((TileBuildSpriteEdit) tabs.getSelectedComponent()).getSprite();
                try {
                    BufferedImage img = ImageIO.read(fc.getSelectedFile());
                    s.referenceImage = img;
                    s.referenceRect = new Rectangle(0, 0, img.getWidth(), img.getHeight());
                    tabs.getSelectedComponent().repaint();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}

}
