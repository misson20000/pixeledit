package misson20000.tools.editsprite;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;

public class TileBuildSpriteEdit extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7250104425040756096L;
	private ButtonGroup framesGroup;
	private JPanel framesPanel;
	private Sprite sprite;
	private TileBuildSpriteViewer view;

	/**
	 * Create the panel.
	 */
	public TileBuildSpriteEdit() {
		this(new Sprite());

	}

	public TileBuildSpriteEdit(Sprite spr) {
		this.sprite = spr;
		setLayout(new BorderLayout());

		framesPanel = new JPanel();
		framesPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null,
				null, null));
		framesPanel.setBackground(Color.DARK_GRAY);
		framesPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		JScrollPane scroll = new JScrollPane(framesPanel);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		add(scroll, BorderLayout.SOUTH);

		JButton btnAddFrame = new RawButton(new ImageIcon(
				TileBuildSpriteEdit.class.getResource("/frameplus.png")));
		btnAddFrame.setActionCommand("add-frame");
		btnAddFrame.addActionListener(this);
		framesPanel.add(btnAddFrame);

		framesGroup = new ButtonGroup();
		ToolGroup g = new ToolGroup();
		TileBuildColors colors = new TileBuildColors();

		TileBuildSpriteViewerContainer viewContainer = new TileBuildSpriteViewerContainer(
				sprite, framesGroup, framesPanel, g, colors);
		view = viewContainer.getViewer();
		colors.setView(view);
		
		JPanel tools = new JPanel();
		tools.setLayout(new GridBagLayout());
		ToolButton b;
		tools.add(b = new ToolButton(Tool.PENCIL, g)); g.bindTool(MouseEvent.BUTTON1, b);
		tools.add(b = new ToolButton(Tool.PICKER, g)); g.bindTool(MouseEvent.BUTTON2, b);
		tools.add(b = new ToolButton(Tool.ERASER, g)); g.bindTool(MouseEvent.BUTTON3, b);
		
		JPanel lpanel = new JPanel();
		lpanel.setLayout(new BoxLayout(lpanel, BoxLayout.Y_AXIS));
		lpanel.add(tools);
		lpanel.add(new JSeparator(JSeparator.HORIZONTAL));
		lpanel.add(colors);
		
		JSplitPane area = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, lpanel, viewContainer);
		area.setOneTouchExpandable(true);
		add(area, BorderLayout.CENTER);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand().equals("add-frame")) {
			TileBuildSpriteFrame b;
			framesPanel.add(b = new TileBuildSpriteFrame(sprite.frames.size(),
					sprite, view), framesPanel.getComponentCount() - 1);
			sprite.newFrame(((TileBuildSpriteFrame.SpriteFrameModel) framesGroup.getSelection())
					.getFrame());
			framesGroup.add(b);
			framesGroup.setSelected(b.getModel(), true);
			b.getModel().setGroup(framesGroup);
			b.doClick();
			this.getParent().repaint();
		}
	}

	public void addFrame(BufferedImage i) {
		TileBuildSpriteFrame b;
		framesPanel.add(b = new TileBuildSpriteFrame(sprite.frames.size(),
				sprite, view), framesPanel.getComponentCount() - 1);
		framesGroup.add(b);
		framesGroup.setSelected(b.getModel(), true);
		b.getModel().setGroup(framesGroup);
		b.doClick();
		sprite.newFrame(i);
		this.getParent().repaint();
	}

	public Sprite getSprite() {
		return sprite;
	}

	public void invalidateViewer() {
		view.invalidate();
	}
}
