package misson20000.tools.editsprite;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

public class ToolButton extends JToggleButton implements MouseListener {
	private Tool tool;
	private ToolGroup toolGroup;
	public static BufferedImage mouseicons[] = new BufferedImage[8];
	
	
	public ToolButton(Tool tool, ToolGroup g) {
		this.tool = tool;
		this.toolGroup = g;
		this.addMouseListener(this);
		this.setFocusPainted(false);
		this.setRolloverEnabled(false);
		if(mouseicons[0] == null) {
			try {
				for(int i = 0; i < 8; i++) {
					mouseicons[i] = ImageIO.read(ToolButton.class.getResourceAsStream("/mouse/" + i + ".png"));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.setIcon(new ImageIcon(ToolButton.class.getResource("/tools/" + tool.name().toLowerCase() + ".png")));
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(mouseicons[toolGroup.getMask(this)], 0, 0, 14, 24, null);
	}
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4551592728816932162L;


	public Tool getTool() {
		return tool;
	}

	@Override
	public void mouseClicked(MouseEvent evt) {
		toolGroup.bindTool(evt.getButton(), this);
	}

	@Override
	public void mouseEntered(MouseEvent evt) {
		
	}

	@Override
	public void mouseExited(MouseEvent evt) {
		
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		
	}

}
