package misson20000.tools.editsprite;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

public class TileBuildSpriteSheetImport extends JDialog implements
		ItemListener, ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5434639443903127143L;
	private JCheckBox alphaCheck;
	private JFormattedTextField alphaField;
	private final JPanel contentPanel = new JPanel();
	private JFileChooser fc;
	private JSpinner hspinner;
	private JLabel lblTransparencyColor;
	private JTabbedPane tabs;
	private JSpinner wspinner;

	/**
	 * Create the dialog.
	 */
	public TileBuildSpriteSheetImport(JFileChooser fc, JTabbedPane tabs) {
		this.fc = fc;
		this.tabs = tabs;
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_contentPanel.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_contentPanel.columnWeights = new double[] { 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_contentPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblSpriteWidth = new JLabel("Sprite Width");
			lblSpriteWidth.setMinimumSize(lblSpriteWidth.getPreferredSize());
			GridBagConstraints gbc_lblSpriteWidth = new GridBagConstraints();
			gbc_lblSpriteWidth.anchor = GridBagConstraints.WEST;
			gbc_lblSpriteWidth.insets = new Insets(0, 0, 5, 5);
			gbc_lblSpriteWidth.gridx = 1;
			gbc_lblSpriteWidth.gridy = 0;
			contentPanel.add(lblSpriteWidth, gbc_lblSpriteWidth);
		}
		{
			wspinner = new JSpinner();
			wspinner.setModel(new SpinnerNumberModel(16, 1, 256, 1));
			wspinner.setMinimumSize(wspinner.getPreferredSize());
			GridBagConstraints gbc_wspinner = new GridBagConstraints();
			gbc_wspinner.fill = GridBagConstraints.HORIZONTAL;
			gbc_wspinner.anchor = GridBagConstraints.EAST;
			gbc_wspinner.insets = new Insets(0, 0, 5, 0);
			gbc_wspinner.gridx = 2;
			gbc_wspinner.gridy = 0;
			contentPanel.add(wspinner, gbc_wspinner);
		}
		{
			JLabel lblSpriteHeight = new JLabel("Sprite Height");
			lblSpriteHeight.setMinimumSize(lblSpriteHeight.getPreferredSize());
			GridBagConstraints gbc_lblSpriteHeight = new GridBagConstraints();
			gbc_lblSpriteHeight.insets = new Insets(0, 0, 5, 5);
			gbc_lblSpriteHeight.anchor = GridBagConstraints.WEST;
			gbc_lblSpriteHeight.gridx = 1;
			gbc_lblSpriteHeight.gridy = 1;
			contentPanel.add(lblSpriteHeight, gbc_lblSpriteHeight);
		}
		{
			hspinner = new JSpinner();
			hspinner.setModel(new SpinnerNumberModel(16, 1, 256, 1));
			hspinner.setMinimumSize(hspinner.getPreferredSize());
			GridBagConstraints gbc_hspinner = new GridBagConstraints();
			gbc_hspinner.fill = GridBagConstraints.HORIZONTAL;
			gbc_hspinner.anchor = GridBagConstraints.EAST;
			gbc_hspinner.insets = new Insets(0, 0, 5, 0);
			gbc_hspinner.gridx = 2;
			gbc_hspinner.gridy = 1;
			contentPanel.add(hspinner, gbc_hspinner);
		}
		contentPanel.setMinimumSize(contentPanel.getPreferredSize());
		{
			alphaCheck = new JCheckBox("");
			GridBagConstraints gbc_alphaCheck = new GridBagConstraints();
			gbc_alphaCheck.anchor = GridBagConstraints.WEST;
			gbc_alphaCheck.insets = new Insets(0, 0, 5, 5);
			gbc_alphaCheck.gridx = 0;
			gbc_alphaCheck.gridy = 2;
			alphaCheck.addItemListener(this);
			contentPanel.add(alphaCheck, gbc_alphaCheck);
		}
		{
			alphaField = new JFormattedTextField(createColorFormatter());
			alphaField.setEnabled(false);
			alphaField.setText("000000");
			alphaField.setMinimumSize(alphaField.getPreferredSize());
			GridBagConstraints gbc_alphaField = new GridBagConstraints();
			gbc_alphaField.anchor = GridBagConstraints.EAST;
			gbc_alphaField.insets = new Insets(0, 0, 5, 0);
			gbc_alphaField.gridx = 2;
			gbc_alphaField.gridy = 2;
			contentPanel.add(alphaField, gbc_alphaField);
			alphaField.setColumns(6);
		}
		{
			lblTransparencyColor = new JLabel("Transparency Color");
			lblTransparencyColor.setEnabled(false);
			GridBagConstraints gbc_lblTransparencyColor = new GridBagConstraints();
			gbc_lblTransparencyColor.anchor = GridBagConstraints.WEST;
			gbc_lblTransparencyColor.insets = new Insets(0, 0, 5, 5);
			gbc_lblTransparencyColor.gridx = 1;
			gbc_lblTransparencyColor.gridy = 2;
			contentPanel.add(lblTransparencyColor, gbc_lblTransparencyColor);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("Ok");
				okButton.addActionListener(this);
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				cancelButton.addActionListener(this);
				buttonPane.add(cancelButton);
			}
		}
		this.pack();
		this.setMinimumSize(this.getSize());
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand() == "Ok") {
			try {
				TileBuildSpriteEdit e;
				tabs.add(fc.getSelectedFile().getName(),
						e = new TileBuildSpriteEdit());
				BufferedImage i = Util.convertColorModel(
						ImageIO.read(fc.getSelectedFile()),
						BufferedImage.TYPE_INT_ARGB);
				Sprite s = e.getSprite();
				int w = s.width = (Integer) wspinner.getModel().getValue();
				int h = s.height = (Integer) wspinner.getModel().getValue();
				boolean colorToAlpha = alphaCheck.isSelected();
				Color toAlpha = null;
				if (colorToAlpha) {
					toAlpha = Color.decode("#" + alphaField.getText());
				}
				for (int x = 0; x < i.getWidth() / w; x++) {
					for (int y = 0; y < i.getHeight() / h; y++) {
						if (colorToAlpha) {
							e.addFrame(toAlpha(
									i.getSubimage(x * w, y * h, w, h), toAlpha));
						} else {
							e.addFrame(i.getSubimage(x * w, y * h, w, h));
						}
					}
				}
				e.invalidateViewer();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
		}
		this.setVisible(false);
	}

	private MaskFormatter createColorFormatter() {
		MaskFormatter formatter = null;
		try {
			formatter = new MaskFormatter("HHHHHH");
		} catch (java.text.ParseException exc) {
			System.err.println("formatter is bad: " + exc.getMessage());
			System.exit(-1);
		}
		return formatter;
	}

	@Override
	public void itemStateChanged(ItemEvent evt) {
		if (evt.getItemSelectable() == alphaCheck) {
			JCheckBox e = (JCheckBox) evt.getItemSelectable();
			this.lblTransparencyColor.setEnabled(e.isSelected());
			this.alphaField.setEnabled(e.isSelected());
		}
	}

	private BufferedImage toAlpha(BufferedImage img, Color toAlpha) {
		int dat[] = new int[img.getWidth() * img.getHeight()];
		img.getRGB(0, 0, img.getWidth(), img.getHeight(), dat, 0,
				img.getWidth());
		for (int i = 0; i < dat.length; i++) {
			if (dat[i] == toAlpha.getRGB()) {
				dat[i] = 0;
			}
		}
		img.setRGB(0, 0, img.getWidth(), img.getHeight(), dat, 0,
				img.getWidth());
		return img;
	}
}
