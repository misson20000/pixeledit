package misson20000.tools.editsprite;

import java.awt.event.MouseEvent;

public class ToolGroup {
	private ToolButton boundtools[] = new ToolButton[3];
	
	public void bindTool(int button, ToolButton t) {
		int c = convertButton(button);
		ToolButton old = boundtools[c];
		boundtools[c] = t;
		if(old != null) {
			if(getMask(old) == 0) old.setSelected(false);
			old.repaint();
		}
		t.setSelected(true);
		t.repaint();
	}
	
	private int convertButton(int button) {
		if(button == MouseEvent.BUTTON1) {
			return 0;
		} else if(button == MouseEvent.BUTTON2) {
			return 1;
		} else {
			return 2;
		}
	}

	public Tool getTool(int button) {
		return boundtools[convertButton(button)].getTool();
	}
	
	public int isBound(ToolButton t) {
		for(int i = 0; i < boundtools.length; i++) {
			if(boundtools[i] == t) {
				return i;
			}
		}
		return -1;
	}
	
	public int getMask(ToolButton t) {
		return    ((boundtools[0] == t ? 1 : 0))
				+ ((boundtools[1] == t ? 2 : 0))
				+ ((boundtools[2] == t ? 4 : 0));
	}
}
