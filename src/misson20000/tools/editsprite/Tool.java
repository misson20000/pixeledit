package misson20000.tools.editsprite;

import java.awt.image.BufferedImage;

public enum Tool {
	PENCIL(true), PICKER, ERASER;

	private boolean preview;
	
	private Tool(boolean preview) {
		this.preview = preview;
	}
	
	private Tool() {
		this(false);
	}
	
	public void perform(BufferedImage img, int x, int y,
			TileBuildColors colors) {
		switch(this) {
		case PENCIL:
			img.setRGB(x, y, colors.getSelectedColor());
			break;
		case ERASER:
			img.setRGB(x, y, 0);
			break;
		case PICKER:
			colors.setSelectedColor(img.getRGB(x, y));
		}
	}

	public boolean showsPreview() {
		return preview;
	}
}
