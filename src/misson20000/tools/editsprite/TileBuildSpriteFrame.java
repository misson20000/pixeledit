package misson20000.tools.editsprite;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;

public class TileBuildSpriteFrame extends JRadioButton implements
		ActionListener {

	public class SpriteFrameModel extends JRadioButton.ToggleButtonModel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 9132452063083422664L;
		private int f;
		private Sprite spr;

		public SpriteFrameModel(int frame, Sprite spr) {
			super();
			f = frame;
			this.spr = spr;
		}

		public int getFrame() {
			return f;
		}

		public Sprite getSprite() {
			return spr;
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1739717830147448481L;
	private int frame;
	private Sprite spr;

	private Component viewer;

	public TileBuildSpriteFrame(int frame, Sprite spr, Component viewer) {
		super();
		this.spr = spr;
		this.frame = frame;
		this.viewer = viewer;
		this.setModel(new SpriteFrameModel(frame, spr));
		this.setBorder(null);
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		this.getParent().repaint();
		this.viewer.repaint();
		this.repaint();
	}

	@Override
	public Dimension getMaximumSize() {
		return new Dimension(38, 38);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(38, 38);
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(this.getParent().getBackground());
		g.fillRect(0, 0, getWidth(), getHeight());
		g.drawImage(spr.frames.get(frame), 3, 3, 32, 32, null);
		g.setColor(Color.GRAY);
		if (this.isSelected()) {
			g.drawRoundRect(0, 0, 37, 37, 5, 5);
		}
	}
}
