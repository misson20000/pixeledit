package misson20000.tools.editsprite;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class RawButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1966915753252212003L;

	public RawButton(ImageIcon ico) {
		super(ico);
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);
		this.setFocusable(false);
	}

	@Override
	public Dimension getMaximumSize() {
		return new Dimension(38, 38);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(38, 38);
	}

}
