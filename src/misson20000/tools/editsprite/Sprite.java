package misson20000.tools.editsprite;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Sprite {
	public ArrayList<BufferedImage> frames;
	public int height;
	public int nframes;
	public int width;
    public BufferedImage referenceImage;
    public Rectangle referenceRect;

	public Sprite() {
		frames = new ArrayList<BufferedImage>();
		width = 16;
		height = 16;
	}

	public void newFrame(BufferedImage i) {
		frames.add(i);
	}

	public void newFrame(int f) {
		BufferedImage i;
		i = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		if (frames.size() != 0) {
			Graphics g = i.getGraphics();
			g.drawImage(frames.get(f), 0, 0, null);
			g.dispose();
		}
		newFrame(i);
	}
}
