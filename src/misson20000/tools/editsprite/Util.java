package misson20000.tools.editsprite;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Util {
	public static BufferedImage convertColorModel(BufferedImage src,
			int bufImgType) {
		BufferedImage img = new BufferedImage(src.getWidth(), src.getHeight(),
				bufImgType);
		Graphics2D g2d = img.createGraphics();
		g2d.drawImage(src, 0, 0, null);
		g2d.dispose();
		return img;
	}

	public static Color opposite(Color in) {
		return new Color(255 - in.getRed(), 255 - in.getGreen(),
				255 - in.getBlue());
	}
}
