package misson20000.tools.editsprite;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

public class TileBuildColors extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 908832439443729492L;
	private int selectedColor= 0xFF000000;
	private TileBuildSpriteViewer view;
	private ButtonGroup buttonGroup;
    private ArrayList<JRadioButton> buttons;
    private int selectedButton;

    public TileBuildColors() {
		JPanel upper = new JPanel();
		ColorIcon icon = new ColorIcon();
		upper.setLayout(new GridLayout(0, 4));
        buttons = new ArrayList<JRadioButton>();
        buttonGroup = new ButtonGroup();
		upper.add(initButton(Color.RED, icon));
		upper.add(initButton(Color.GREEN, icon));
		upper.add(initButton(Color.BLUE, icon));
		upper.add(initButton(Color.BLACK, icon));
		upper.add(initButton(Color.WHITE, icon));
		
		JPanel lower = new JPanel();
        {
            JButton edit = new JButton("Edit");
            edit.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    buttons.get(selectedButton).setForeground(new Color(68, 255, 15));
                }
            });
            lower.add(edit);
        }
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(upper);
		add(lower);
	}
	
	private JRadioButton initButton(Color color, ColorIcon icon) {
		JRadioButton button = new ColorButton(icon);
		button.setForeground(color);
		button.setActionCommand(Integer.toString(buttons.size()));
		button.addActionListener(this);
        buttonGroup.add(button);
        buttons.add(button);
		return button;
	}
	
	public void setView(TileBuildSpriteViewer view) {
		this.view = view;
	}
	
	public int getSelectedColor() {
		return selectedColor;
	}

	public void setSelectedColor(int c) {
		this.selectedColor = c;
		this.repaint();
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
        selectedButton = Integer.parseInt(evt.getActionCommand());
        selectedColor = buttons.get(selectedButton).getForeground().getRGB() | 0xFF000000;
	}

    private class ColorButton extends JRadioButton {
        public ColorButton(ColorIcon icon) {
            super();
            this.setRolloverEnabled(false);
        }

        @Override
        public void paint(Graphics g) {
            g.setColor(this.getForeground());
            g.fillRect(0, 0, getWidth(), getHeight());
            super.paint(g);
            if(this.isSelected()) {
                g.setColor(this.getForeground());
                g.drawRect(1, 1, getWidth()-2, getHeight()-2);
            }
        }
    }
}
