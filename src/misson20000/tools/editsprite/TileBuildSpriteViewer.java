package misson20000.tools.editsprite;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

import javax.swing.*;

public class TileBuildSpriteViewer extends Canvas implements MouseListener,
		MouseMotionListener, Scrollable, AdjustmentListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3123426289203774985L;
	private ButtonGroup fgroup;
	private Container frameButtons;
	private int mx;
	private int my;
	private Sprite spr;
	private int zoom;
	private ToolGroup toolGroup;
	private TileBuildColors colors;
	private boolean mdown;
	private int mbutton;
    private int scrollx;
    private int scrolly;

    public TileBuildSpriteViewer(Sprite spr, ButtonGroup framesGroup,
			Container frameButtons, ToolGroup toolGroup, TileBuildColors colors) {
		this.spr = spr;
		this.fgroup = framesGroup;
		this.frameButtons = frameButtons;
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.zoom = 8;
		this.colors = colors;
		this.toolGroup = toolGroup;
	}

	@Override
	public Dimension getMaximumSize() {
		return new Dimension(zoom * spr.width, zoom * spr.height);
	}

	@Override
	public void mouseClicked(MouseEvent evt) {

	}

	@Override
	public void mouseDragged(MouseEvent evt) {
		if (fgroup.getSelection() != null) {
			toolGroup.getTool(mbutton).perform(spr.frames.get(((TileBuildSpriteFrame.SpriteFrameModel) fgroup
					.getSelection()).getFrame()), evt.getX() / zoom, evt.getY() / zoom, colors);
				this.frameButtons.getComponent(
					((TileBuildSpriteFrame.SpriteFrameModel) fgroup
							.getSelection()).getFrame()).repaint();
			this.repaint();
		}
		mouseMoved(evt);
	}

	@Override
	public void mouseEntered(MouseEvent evt) {
		mx = (evt.getX()-scrollx) / zoom;
		my = (evt.getY()-scrolly) / zoom;
		repaint();
	}

	@Override
	public void mouseExited(MouseEvent evt) {
		mx = -1;
		my = -1;
		repaint();
	}

	@Override
	public void mouseMoved(MouseEvent evt) {
		if (evt.getX() / zoom != mx || evt.getY() / zoom != my) {
			this.repaint();
		}
		mx = (evt.getX()-scrollx) / zoom;
		my = (evt.getY()-scrolly) / zoom;
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		if (fgroup.getSelection() != null) {
			toolGroup.getTool(evt.getButton()).perform(spr.frames.get(((TileBuildSpriteFrame.SpriteFrameModel) fgroup
					.getSelection()).getFrame()), evt.getX() / zoom, evt.getY() / zoom, colors);

			this.frameButtons.getComponent(
					((TileBuildSpriteFrame.SpriteFrameModel) fgroup
							.getSelection()).getFrame()).repaint();
			this.repaint();
		}
		this.mbutton = evt.getButton();
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		this.mbutton = 0;
	}

	public void setZoom(int z) {
		zoom = z;
		this.revalidate();
		this.repaint();
	}

	@Override
	public void update(Graphics origg) {
		if (fgroup.getSelection() != null) {
			BufferedImage buf = new BufferedImage(Math.min(spr.width*zoom, this.getWidth()), Math.min(spr.height*zoom, this.getHeight()), BufferedImage.TYPE_INT_RGB);
			Graphics2D g = buf.createGraphics();
			g.setColor(Color.WHITE);
			for (int x = 0; x <= getWidth(); x += 8) {
				if (x % 16 == 0) {
					g.setColor(Color.LIGHT_GRAY);
				} else {
					g.setColor(Color.WHITE);
				}
				for (int y = 0; y <= getHeight(); y += 8) {
					g.fillRect(x - (scrollx%8), y - (scrolly%8), 8, 8);
					if (g.getColor().equals(Color.WHITE)) {
						g.setColor(Color.LIGHT_GRAY);
					} else {
						g.setColor(Color.WHITE);
					}
				}
			}
            if(spr.referenceImage != null) {
                g.drawImage(spr.referenceImage, 0-scrollx, 0-scrolly, spr.width * zoom, spr.height * zoom, spr.referenceRect.x, spr.referenceRect.y, spr.referenceRect.width + spr.referenceRect.x, spr.referenceRect.height + spr.referenceRect.y, null);
            }
			g.drawImage(spr.frames
					.get(((TileBuildSpriteFrame.SpriteFrameModel) fgroup
							.getSelection()).getFrame()), 0-scrollx, 0-scrolly, spr.width
					* zoom, spr.height * zoom, null);
			if (toolGroup.getTool(MouseEvent.BUTTON1).showsPreview() && mx != -1 && my != -1) {
				g.setColor(new Color(colors.getSelectedColor()));
				g.fillRect(mx * zoom, my * zoom, zoom, zoom);
			}
			if (zoom >= 5) {
				g.setColor(Color.LIGHT_GRAY);
				g.drawRect(mx * zoom, my * zoom, zoom, zoom);
			}
			g.dispose();
			origg.drawImage(buf, Math.max(0, getWidth()/2-spr.width*zoom/2), Math.max(0, getHeight()/2-spr.height*zoom/2), null);
		}
	}

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return null;
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 1;
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        return (orientation == SwingConstants.HORIZONTAL) ? spr.width -1 : spr.height -1;
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return false;
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return false;
    }

    @Override
    public void adjustmentValueChanged(AdjustmentEvent e) {
        if(((JScrollBar)(e.getAdjustable())).getName().equals("Vertical")) {
            scrolly = e.getValue();
        } else {
            scrollx = e.getValue();
        }
    }
}
