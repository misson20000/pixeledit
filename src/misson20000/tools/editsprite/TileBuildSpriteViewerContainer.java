package misson20000.tools.editsprite;

import java.awt.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class TileBuildSpriteViewerContainer extends JPanel implements
		ChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2165101265217830989L;
	private TileBuildSpriteViewer view;
	private JSpinner zoom;
	private JPanel zoompanel;

	public TileBuildSpriteViewerContainer(Sprite sprite,
			ButtonGroup buttonGroup, Container buttonPanel, ToolGroup toolGroup, TileBuildColors colors) {
		this.setLayout(new BorderLayout());
		view = new TileBuildSpriteViewer(sprite, buttonGroup, buttonPanel, toolGroup, colors);
        JPanel scroll = new JPanel();
        scroll.setLayout(new BorderLayout());
        scroll.add(view, BorderLayout.CENTER);

        JScrollBar h = new JScrollBar();
        h.setOrientation(JScrollBar.HORIZONTAL);
        h.addAdjustmentListener(view);
        h.setName("Horizontal");
        h.setMinimum(0);
        h.setUnitIncrement(1);
        h.setMaximum(sprite.width * 16 - view.getWidth());
        scroll.add(h, BorderLayout.SOUTH);

        JScrollBar v = new JScrollBar();
        v.setOrientation(JScrollBar.VERTICAL);
        v.addAdjustmentListener(view);
        v.setName("Vertical");
        v.setMinimum(0);
        v.setMaximum(sprite.height * 16 - view.getHeight());
        scroll.add(v, BorderLayout.EAST);

		this.add(scroll, BorderLayout.CENTER);
		{
			zoompanel = new JPanel();
			zoompanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				JLabel label = new JLabel("Zoom");
				zoompanel.add(label);
			}
			zoom = new JSpinner(new SpinnerNumberModel(16, 1, 16, 1));
			zoompanel.add(zoom);
			zoom.addChangeListener(this);
			zoompanel.setMaximumSize(view.getMaximumSize());
			this.add(zoompanel, BorderLayout.SOUTH);
		}
		this.setVisible(true);
	}

	public TileBuildSpriteViewer getViewer() {
		return view;
	}

	@Override
	public void stateChanged(ChangeEvent evt) {
		view.setZoom((Integer) zoom.getModel().getValue());
		zoompanel.setMaximumSize(view.getMaximumSize());
		zoompanel.revalidate();
	}
}
